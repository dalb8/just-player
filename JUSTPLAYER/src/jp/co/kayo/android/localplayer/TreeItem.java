package jp.co.kayo.android.localplayer;

public class TreeItem {
    public int groupPosition;
    public int childPosition;
    
    public TreeItem(int g, int c){
        groupPosition = g;
        childPosition = c;
    }
    
    public TreeItem(int g){
        groupPosition = g;
        childPosition = -1;
    }

    @Override
    public boolean equals(Object o) {
        TreeItem obj = (TreeItem)o;
        return obj.groupPosition == groupPosition && obj.childPosition == childPosition;
    }
    
    
}
